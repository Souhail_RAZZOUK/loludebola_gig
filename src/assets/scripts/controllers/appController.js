(function() {
  
  "use strict";
  
  uiControllers.controller("appController",  ['$window', '$rootScope', '$scope', '$location', 'View', function ($window, $rootScope, $scope, $location, View) {
    $scope.splitViewElement = document.getElementById("splitView");
    $scope.View = View;
    
    View.setTitle("Home");

    $scope.goTo = function(pageUri) {
      $location.path(pageUri);
      if($scope.splitViewElement.winControl._isOpenedMode){
        $scope.splitViewElement.winControl.closePane();
      }
    }

    $scope.hideUserMenu = function() {
      var userMenu = document.getElementById('user-menu');
      userMenu.winControl.hide();
    }
    
    $scope.showUserMenu = function() {
      var anchor = document.getElementById("user-credits-wrapper");
      var userMenu = document.getElementById('user-menu').winControl;
      if($scope.splitViewElement.winControl._isOpenedMode){
        userMenu.show(anchor, 'bottom');
        return;
      }
      userMenu.show(anchor, 'right');
    }

    $scope.toggleUserMenu = function() {
      var userMenu = document.getElementById('user-menu');
      if(userMenu.winControl.hidden){
        $scope.showUserMenu();
        return
      }
      $scope.hideUserMenu();
    }

    $scope.updateSplitViewDisplayMode = function(breakpoint) {
      $scope.splitViewElement.winControl.openedDisplayMode = appSplitViewModes.mode[breakpoint].openedDisplayMode;
      $scope.splitViewElement.winControl.closedDisplayMode = appSplitViewModes.mode[breakpoint].closedDisplayMode;
    }

    $rootScope.$on("breakpoint-change", function(event, breakpoint) {
      $scope.updateSplitViewDisplayMode(breakpoint);
    });

    $rootScope.$on("split-view:initialised", function(e) {
      var breakpoint = getCurrentBreakpoint();
      $scope.updateSplitViewDisplayMode(breakpoint);      
    });

  }]);
  
  
}).call(this);