(function() {
  
  "use strict";
  
  taskslistsControllers.controller('taskController', ['$rootScope', '$scope', '$routeParams', 'Tasks', 'View',
    function($rootScope, $scope, $routeParams, Tasks, View) {
    
      
      $scope.task = Tasks.get($routeParams.taskId);

      $scope.backToList =  function() {
        $scope.$parent.goTo('/taskslist/' + $scope.task.details.listId);
      };

      // backButton.refresh();
    }]);
  
}).call(this);
