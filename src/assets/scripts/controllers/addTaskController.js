(function() {

  "use strict";

  taskslistsControllers.controller('addTaskController', [ '$rootScope', '$scope', 'Tasks',
    function( $rootScope, $scope, Tasks ) {

      $scope.resetTask = function() {
        $scope.newTask = {
          title: "",
          done: "false",
          dueDate: ""
        };
      }

      $scope.resetTask();

      $scope.addTask = function() {
        var newTitle = $scope.newTask.title = $scope.newTask.title.trim();
        if (newTitle.length === 0) {
          return;
        }
        // $scope.updateDueDate();
        $scope.datasource.tasks.push($scope.newTask);
        Tasks.add($scope.datasource.details.id,$scope.newTask);
        $scope.resetTask();
      }

      $scope.updateDueDate = function() {
        var tempDateTime = "",
            UTCMinutes = ($scope.dueTimeElm.winControl.current.getUTCMinutes().toString().length < 2) ? '0' + $scope.dueTimeElm.winControl.current.getUTCMinutes().toString() : $scope.dueTimeElm.winControl.current.getUTCMinutes().toString();

        tempDateTime += $scope.dueDateElm.winControl.current.getUTCDate()
                + " " + appDateComponents.months[$scope.dueDateElm.winControl.current.getUTCMonth()].substr(0,3)
                + " " + $scope.dueDateElm.winControl.current.getUTCFullYear()
                + " " + $scope.dueTimeElm.winControl.current.getUTCHours()
                + ":" + UTCMinutes + " GMT";

        $scope.newTask.dueDate = tempDateTime;
      }

      $scope.revertDueDate = function() {
        $scope.newTask.dueDate = "";
      }

      $rootScope.$on("due-date-dialog:initialised", function(event) {
        $scope.dueDateElm = document.getElementById('dueDate');
        $scope.dueTimeElm = document.getElementById('dueTime');
      });

    }]);

}).call(this);