(function() {
  
  "use strict";
  
  taskslistsControllers.controller('taskslistsController', ['$rootScope', '$scope', '$location', 'Taskslists', 'View',
    function($rootScope, $scope, $location, Taskslists, View) {
    
      $scope.taskslistsGridView = document.getElementById("taskslistsGridView");
      View.setTitle("Home");

      $scope.goToDetails = function (taskslistId){
        $location.path("/taskslist/"+ taskslistId);
      }

      Taskslists.getAll().then(function(taskslists){
        $scope.taskslists = taskslists;
      });

      $rootScope.$on('breakpoint-change', function() {
        $scope.taskslistsGridView.winControl.forceLayout();
      });
    
    }]);
  
}).call(this);
