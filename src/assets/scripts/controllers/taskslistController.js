(function() {

  "use strict";

  taskslistsControllers.controller('taskslistController', ['$scope', '$resource', '$rootScope', '$filter', '$routeParams', 'Taskslists', 'View',
    function($scope, $resource, $rootScope, $filter, $routeParams, Taskslists, View) {

      $scope.taskslistToolBar = document.getElementsByClassName('taskslist-tool-bar')[0];
      $scope.tasksListView = document.getElementById('tasks-list-view');
      $scope.selectedTasks = [];
      $scope.selectedIndexes = [];

      var mapSelectedIndexes = function() {
        $scope.selectedTasks = $scope.selectedIndexes.map(function(el) {
          return $scope.taskslist.tasks[el];
        });
      }

      var refreshToolBar = function() {
        var multiSelectionCommands = [
          $scope.taskslistToolBar.winControl.getCommandById('deleteCommand'),
          $scope.taskslistToolBar.winControl.getCommandById('markDoneCommand')
        ];

        if($scope.selectedIndexes.length > 0){
          multiSelectionCommands.forEach(command => command.hidden = false);
        }else{
          multiSelectionCommands.forEach(command => command.hidden = true);
        }
      }

      var updateTaskslistTitleDisplay = function(breakpoint) {
        var taskslistTitleComm = $scope.taskslistToolBar.winControl.getCommandById('taskslist-title');
        if(breakpoint === "small"){
          $scope.taskslistToolBar.winControl.getCommandById('taskslist-title').hidden = true;
        }else {
          $scope.taskslistToolBar.winControl.getCommandById('taskslist-title').hidden = false;
        }
      }

      $scope.taskslist = Taskslists.get($routeParams.taskslistId);
      // View.setTitle($scope.taskslist.details.title);

      $scope.$watch('taskslist.tasks', function () {
      }, true);

      $scope.$watch('selectedIndexes', function () {
        refreshToolBar();
        mapSelectedIndexes()
      }, true);

      $scope.markTaskAsDone = function(taskId) {
        $scope.taskslist.tasks.some(function(task) {
          if(task.id == taskId){
            return task.done = true;
          }
        });
      }

      $scope.markSelectedTasksAsDone = function() {
        angular.forEach($scope.selectedTasks, function (task, index) {
          $scope.markTaskAsDone(task.id);
        });
        $scope.selectedIndexes = [];        
      }

      $scope.deleteTask = function(taskId) {
        taskId = "!" + taskId;
        $scope.taskslist.tasks = $filter('filter')($scope.tasks, {id:taskId});
      }

      $scope.deleteSelectedTasks = function() {
        angular.forEach($scope.selectedTasks, function (task, index) {
          $scope.deleteTask(task.id);
        });
        $scope.selectedIndexes = [];
      }

      $scope.showSortMenu = function(event) {
        var sortMenu = document.getElementById('sort-tasks-menu').winControl;
        var sortCommand = $scope.taskslistToolBar.winControl.getCommandById("sortCommand");
        // if(sortCommand.)
        sortMenu.show(event.target, 'bottom');
      }

      $scope.showTaskMenu = function(event, id) {
        var currentTaskMenu = document.getElementById('task-menu-' + id);
        if(currentTaskMenu && currentTaskMenu.winControl){
          currentTaskMenu.winControl.show(event.target,'left');
        }
      }

      $scope.goToTask = function(id) {
        $scope.$parent.goTo('/task/' + id);
      }

      $rootScope.$on("breakpoint-change", function(event, breakpoint) {
        refreshToolBar();
        updateTaskslistTitleDisplay(breakpoint);
        $scope.tasksListView.winControl.forceLayout();
      });

      $rootScope.$on("taskslist-tool-bar:initialised", function(event) {
        var breakpoint = getCurrentBreakpoint();
        updateTaskslistTitleDisplay(breakpoint);
      });

    }]);

}).call(this);