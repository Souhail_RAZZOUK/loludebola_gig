// // Array Remove - By John Resig (MIT Licensed)
// Array.prototype.remove = function(start, to) {
//   var rest = this.slice((to || start) + 1 || this.length);
//   this.length = start < 0 ? this.length + start : start;
//   return this.push.apply(this, rest);
// };

var getCurrentBreakpoint = function() {
  return window.getComputedStyle(document.body, ':after').getPropertyValue('content').replace(/^["']|["']$/g, '');
}

var loludebolaApp = angular.module('Loludebola', [
                                        'winjs',
                                        'ngRoute', 
                                        'uiControllers', 
                                        'taskslistsControllers',
                                        'taskslistServices',
                                        'mediaQueriesWatcher',
                                        'initializeWatcher',
                                        'addTask',
                                        'alarm',
                                        'dataServices',
                                        'uiServices'
                                        ]);
                                        
var uiControllers = angular.module('uiControllers', []);
var taskslistsControllers = angular.module('taskslistsControllers', []);
var taskslistServices = angular.module('taskslistServices', ['ngResource']);
var dataServices = angular.module('dataServices', ['ngResource']);
var uiServices = angular.module('uiServices', []);
var mediaQueriesWatcher = angular.module('mediaQueriesWatcher', []);
// var autoFocus = angular.module('autoFocus', []);
var initializeWatcher = angular.module('initializeWatcher', []);
var alarm = angular.module('alarm', ['ngAudio', 'toaster']);
var addTask = angular.module('addTask', []);

WinJS.Namespace.define("appDateComponents", {
  months: {
    0:"January",
    1:"February",
    2:"March",
    3:"April",
    4:"May",
    5:"June",
    6:"July",
    7:"August",
    8:"September",
    9:"October",
    10:"November",
    11:"December"
  }
});

WinJS.Namespace.define("appSplitViewModes", {
  mode: {
    small: {
      name: 'small',
      openedDisplayMode: WinJS.UI.SplitView.OpenedDisplayMode.overlay,
      closedDisplayMode: WinJS.UI.SplitView.ClosedDisplayMode.none,
    },
    medium: {
      name: 'medium',
      openedDisplayMode: WinJS.UI.SplitView.OpenedDisplayMode.overlay,
      closedDisplayMode: WinJS.UI.SplitView.ClosedDisplayMode.inline,
    },
    largeAndUp: {
      name: 'large-and-up',
      openedDisplayMode: WinJS.UI.SplitView.OpenedDisplayMode.inline,
      closedDisplayMode: WinJS.UI.SplitView.ClosedDisplayMode.inline,
    }
  }
});

WinJS.Namespace.define("appDataUrls", {
  users: "",
  taskslists: "",
  tasks: ""
});

loludebolaApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {                        
  $routeProvider                                                                
    .when('/', {                                            
      templateUrl: "views/home.html",                                               
      controller: 'taskslistController'                              
    })
    .when('/taskslist/:taskslistId', {
      templateUrl: "views/taskslist.html",
      controller: 'taskslistController'                              
    })
    .when('/task/:taskId', {
      templateUrl: "views/task.html",
      controller: 'taskController'                            
    })
    .otherwise({                      
        template: '<h1 class="win-h1">404</h1>'   
    });      
  $locationProvider.html5Mode(true);
  
}]);

