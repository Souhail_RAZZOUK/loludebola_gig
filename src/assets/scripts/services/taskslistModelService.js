(function() {

  "use strict";

  taskslistServices.service('Taskslists', ['$q', '$resource', 'Tasks', function($q, $resource, Tasks) {


     //// FUCKING USE 'THIS' YOU FUCKING IDIOT //// 
    var model = this;
    
    this.details = {};
    this.tasks = [];

    this.instance = function(id) {
      if(id) {
        return $resource('http://localhost:3001/api/taskslist/:id', {id:id});      
      }
      return $resource('http://localhost:3001/api/taskslists/', {}, {});
    }
    
    this.get = function(id) {

      model.instance(id).get().$promise.then(function(data){
        model.details = data;
      });

      Tasks.getByTasklistId(id).then(function(tasks) {
        model.tasks = tasks;
      });

      return model;   
    }

    this.getAll = function() {
      var deferred = $q.defer();

      model.instance().query().$promise.then(function(data) {
        deferred.resolve(data);
      });

      return deferred.promise;
    }

    this.create = function(listData) {
      var tasklist = listData;
      model.instance().save(tasklist)
    }

  }]);

}).call(this);