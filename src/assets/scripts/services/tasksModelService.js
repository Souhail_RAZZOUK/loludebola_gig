(function() {

  "use strict";

  taskslistServices.service('Tasks', ['$q', '$resource', 'taskslistTasks', function($q, $resource, taskslistTasks) {

    var model = this;

    model.details = {};
    model.subTasks = [];

    this.instance = function(id) {
      if(id) {
        return $resource('http://localhost:3001/api/task/:id', {id:id}, {});      
      }
      return $resource('http://localhost:3001/api/tasks', {}, {});
    }
    
    this.get = function(id) {
      model.instance(id).get().$promise.then(function(data){
        model.details = data;
      });
      return model;
    }

    this.getAll = function() {
      var deferred = $q.defer();
      model.instance().query().$promise.then(function(data){
        deferred.resolve(data)
      });
      return deferred.promise;
    }

    this.getByTasklistId = function(listId) {   
      var deferred = $q.defer();
      taskslistTasks.query({taskslistId:listId}).$promise.then(function(data){
        deferred.resolve(data)
      });
      return deferred.promise;
    }

    this.add = function(listId, taskData) {
      var instance = model.instance();
      var task = angular.merge({},taskData,{taskslistId:listId})
      model.instance().save(task)
    }

    this.update = function(taskId, taskData) {

    }

    this.remove = function(taskId) {

    }

  }]);

}).call(this);