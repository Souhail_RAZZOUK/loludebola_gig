(function() {

  "use strict";

//   dataServices.factory('taskslists', ['$resource', 
//     function($resource) {
//       return $resource('http://localhost:3001/api/taskslists', {}, {
//                 query:{
//                   method:'GET', 
//                   params:{ }, 
//                   isArray: true
//                 }
//               });
//     }]);

//   dataServices.factory('taskslistDetails', ['$resource', 
//     function($resource) {
//       return $resource('http://localhost:3001/api/taskslist/:taskslistId', {}, {
//                 query:{
//                   method:'GET', 
//                   params:{taskslistId:"@taskslistId" }
//                 }
//               });
//     }]);

  dataServices.factory('taskslistTasks', ['$resource', 
    function($resource) {
      return $resource('http://localhost:3001/api/taskslist/:taskslistId/tasks', {}, {
                query:{
                  method:'GET', 
                  params:{taskslistId:"@taskslistId" },
                  isArray: true
                }
              });
    }]);

//   dataServices.factory('allTaskslists', ['$resource', 
//     function($resource) {
//       return $resource('http://localhost:3001/api/taskslists', {}, {
//                 query:{
//                   method:'GET', 
//                   isArray: true
//                 }
//               });
//     }]);

//   dataServices.factory('allTasks', ['$resource', 
//     function($resource) {
//       return $resource('http://localhost:3001/api/tasks', {}, {
//                 query:{
//                   method:'GET', 
//                   isArray: true
//                 }
//               });
//     }]);

//   dataServices.factory('taskDetails', ['$resource', 
//     function($resource) {
//       return $resource('http://localhost:3001/api/task/:taskId', {}, {
//                 query:{
//                   method:'GET', 
//                 }
//               });
//     }]);

//   dataServices.factory('addTask', ['$resource', 
//     function($resource) {
//       return $resource('http://localhost:3001/api/tasks', {}, {
//                 query:{
//                   method:'POST', 
//                 }
//               });
//     }]);

}).call(this);