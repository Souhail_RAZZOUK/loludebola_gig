(function() {

  "use strict";

  mediaQueriesWatcher.directive('mediaQueriesWatcher', ['$rootScope', '$window' ,function ($rootScope, $window) {

    return {
      restrict: 'A',
      link: function ($scope, $el, $attrs) {
        $scope.$watch(
          function () { 
            var breakpoint = $window.getComputedStyle ? $window.getComputedStyle($el[0], ':after').getPropertyValue('content') : false;
            return breakpoint.replace(/^["']|["']$/g, '');
          },
          function (newValue, oldValue) {
            if (newValue !== oldValue) {
              $rootScope.$broadcast('breakpoint-change', newValue);
              console.log(newValue);
            }
          }
        );
      }
    }

  }]);

}).call(this);

