(function() {

  "use strict";

  addTask.directive('addTask', function () {

    return {
      restrict: 'E',
      scope: {
        datasource: '='
      },
      replace: true,
      templateUrl: 'templates/addTaskDirective.html',
      link: function(scope, elem, attrs) {

        scope.commandsBox = document.querySelector('#add-task .options-box');
        scope.TimeDateDialogHidden = true;

        scope.showTimeDateDialog = function () {
          scope.TimeDateDialogHidden = false;
        };

        scope.showOptionBox = function() {
          scope.commandsBox.classList.remove('ng-hide');
        }

        elem[0].onfocus = function() {
          scope.showOptionBox();
        }

        elem[0].onblur = function() {
          scope.commandsBox.classList.add('ng-hide');
        }

      },
      controller: 'addTaskController'
    }

  });

}).call(this);

